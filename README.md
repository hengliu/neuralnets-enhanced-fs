# NeuralNets Enhanced FS

In this project, we use the Neural Nets to boost the performance of information theoretic FS where subsequent classification performance can be significantly increased. The source codes will be public soon after the manuscript gets accepted.